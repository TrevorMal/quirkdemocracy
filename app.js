var express = require('express');
var port = 3000;
var app = express();
var path = require('path');
var bodyparser = require('body-parser');
var cors = require('cors');
//DB configuration
var mongo = require('mongodb');
var monk = require('monk');

var db = monk('localhost:27017/QuirkDemocracy');

/*//Config app
app.set('view engine','ejs');
app.set('views',path.join(__dirname,'views'));
*/
//Jade configuration
app.set('view engine','jade');
app.set('views',path.join(__dirname,'views'));

//Use middleware
//app.use(bodyparser.json());
//app.use(bodyparser.urlencoded());
app.use(bodyparser());
app.use(express.static(path.join(__dirname,'lib')));
app.use(express.static(path.join(__dirname,'images')));
app.use(function(req,res,next){
	req.db = db;
	next();
});

app.all('*', function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header('Access-Control-Allow-Methods', 'OPTIONS,GET,POST,PUT,DELETE');
	res.header("Access-Control-Allow-Headers", "Content-Type, Authorization, X-Requested-With");
	if ('OPTIONS' == req.method){
		return res.sendStatus(200);
	}
	next();
});
//Define routes

app.use(require('./routers'));

//Error handling
app.use(function(err,req,res,next){
	console.error(err.stack);
	res.status(404).render('error404',{
		pagetitle : ' Error 404'
	});
});

app.use(function(err,req,res,next){
	console.error(err.stack);
	res.status(500).render('error404',{
		pagetitle : ' Error 500'
	});

});


//Start server
app.listen(port,function(){
	console.log('Ready on port 3000');
});