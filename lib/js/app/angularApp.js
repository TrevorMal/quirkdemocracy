/**
 * Created by Trevor Malovhele on 2015/05/27.
 */
'use strict';

angular
    .module('angularApp',['ngRoute'])
    .config(['$routeProvider'],function($routeProvider){
        $routeProvider.otherwise({redirectTo : '/dashboard'});
    })