/**
 * Created by Trevor Malovhele on 2015/05/27.
 */

(function () {
    'use strict';

    angular
        .module('angularApp')
        .config(config);

    function config($routeProvider) {
        $routeProvider
            .when('/liveResults', {
                templateUrl: '/partials/live-result.html',
                controller: 'LiveResultsController',
                controllerAs: 'vm'
            });
    }
}());