$(document).ready(function () {
    //Create and load data from MongoDB
    //var votes = [];
    console.log('Inside JQuery');
    $.ajax({
        url: 'http://localhost:8044/votingResults'
    }).then(function (votes) {
        //console.log(data);
        //votes.push(data);
        console.log(votes);
        $(function () {
            $('#results').highcharts({
                chart: {
                    type: 'column',
                    animation: Highcharts.svg,
                    redraw: {
                        load: function () {

                            var series = this.series[0];
                            setInterval(function () {

                                $.ajax({
                                    url: 'http://localhost:8044/votingResults'
                                }).then(function (res) {

                                    for (var v in res) {
                                        var x = 0;
                                        var y = res[v].votes;
                                        series.addPoint([x, y], true, true);
                                    }
                                });

                            }, 2000);
                        }
                    }
                },
                title: {
                    text: 'Votes '
                },
                subtitle: {
                    text: 'Quirk Democracy'
                },
                xAxis: {
                    title: {text: 'Voters'},
                    category: (function () {
                        var res = [];
                        for (var v in votes) {
                            res.push(
                                votes[v].name
                            );
                        }
                        console.log(res);
                        return res;
                    })
                },
                yAxis: {
                    title: {
                        text: 'Total Votes'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.series.data[0].name + '</b><br/>' +
                            Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                            Highcharts.numberFormat(this.y, 0);
                    }
                },
                series: [{
                    name: 'Test',
                    data: (function () {
                        var res = [];
                        for (var v in votes) {

                            res.push(
                                votes[v].votes
                            );
                        }
                        console.log(res);
                        return res;

                    }())
                }]
            });
        });
    });
});
