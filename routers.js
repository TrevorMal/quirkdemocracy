var express = require('express');
var router = express.Router();
var restify = require('restify');


//Create a JSON client
var client = restify.createJsonClient({
    url: 'https://dali.quirk.biz'
});

router.get('/', function (req, res) {

    res.render('index', {
        pagetitle: 'Welcome to Quirk Democracies.',
        errorMsg: ''
    });
});

router.get('/api/votingResults', function (req, res) {
    var params = req.body;
    var db = req.db;
    var data = [];
    var resultsCollection = db.get('results');
    resultsCollection.find({}, function (error, docs) {
        if (error) throw error;
        //console.log(JSON.stringify(r.candidate_votes));
        for (var d in docs) {
            data[d] = {
                name: docs[d].candidate.first_name,
                votes: docs[d].candidate_votes
            };
        }
        //console.log(data);
        res.json(data);
    });
    //console.log(data.candidate);
});

router.post('/login', function (req, res, next) {
    var params = req.body;
    var db = req.db;
    //console.log(params.userName);
    var options = {
        path: '/api/authenticate?username=' + params.userName + '&pass=' + params.userPassword
    };

    client.get(options, function (api_err, api_req, api_res, obj) {
        //Checks the loggin status
        if (obj.status_message === 'Authentication Successful') {
            var collection = db.get('users');
            collection.findOne({'id': obj.id}, function (e, results) {
                console.log('Testing ' + results);
                if (results !== null) {
                    if (results.role !== '1') {
                        var loggedInUser = results.first_name + ' ' + results.last_name;
                        var loggedInTime = Date.now();
                        res.render('dashboard/mainDashboard', {
                            pagetitle: 'Dashboard | ',
                            user: loggedInUser,
                            time: loggedInTime
                        });
                    }
                }
                else {
                    //Create a new user in the database
                    //collection.insert(obj);
                    //Add the default role
                    //collection.update({'id': results.id}, {$set: {'role': '0'}});
                    //Query the database to get the user.
                    collection.findOne({'id': obj.id}, function (e, results) {
                        //console.log(results.first_name);
                        if (results.role !== '1') {
                            var loggedInUser = results.first_name + ' ' + results.last_name;
                            var loggedInTime = Date.now();
                            res.render('dashboard/mainDashboard', {
                                pagetitle: 'Main Dashboard',
                                user: loggedInUser,
                                time: loggedInTime,
                                data: voteResults
                            });
                        }
                    });
                }
            });
        }
        else {
            console.log('Going back to index')
            res.render('index.jade', {
                pagetitle: 'Welcome to Quirk Democracies.',
                errorMsg: obj.status_message + '!!!'
            });
        }
    });
});

router.get('/logout', function (req, res) {
    res.render('index.jade', {
        pagetitle: 'Welcome to Quirk Democracies.',
        errorMsg: ''
    });
})

router.get('/api/users', function (req, res, next) {
    var db = req.db;
    var collection = db.get('quirkUsers');
    collection.find({}, function (e, docs) {
        if (docs !== null) {
            //console.log(docs);
            res.json(docs);
        }
        else {
            res.send('No Results');
        }

    })
});

/*Remote login*/
router.post('/api/authenticate', function (req, res, next) {
    //console.log(req.body);
    var params = req.body;
    var db = req.db;
    var users = {};

    var options = {
        path: '/api/authenticate?username=' + params.username + '&pass=' + params.pass
    };

    client.get(options, function (api_err, api_req, api_res, obj) {
        //Checks the loggin status
        if (obj.status_message === 'Authentication Successful') {
            var collection = db.get('users');
            collection.findOne({'email_address': params.username}, function (e, results) {
                console.log("User Exists" + results);
                if (results !== null) {
                    if (results.role !== '1') {
                        var loggedInUser = results.first_name + ' ' + results.last_name;
                        var loggedInTime = Date.now();
                        res.json({
                            status_message: results.status_message,
                            user: loggedInUser,
                            time: loggedInTime
                        });
                    }
                }
                else {
                    users = {
                        email_address: params.username,
                        first_name: obj.first_name,
                        last_name: obj.last_name,
                        company: obj.company,
                        team: obj.team,
                        'role': 'ADMIN'
                    };
                    console.log(users);
                    //Create a new user in the database
                    collection.insert(users);
                    //Query the database to get the user.
                    collection.findOne({'email_address': params.username}, function (e, results) {
                        console.log('Check Roll' + results);
                        if (results != null) {
                            var loggedInUser = results.first_name + ' ' + results.last_name;
                            var loggedInTime = Date.now();
                            res.json({
                                status_message: obj.status_message,
                                user: loggedInUser,
                                time: loggedInTime
                            });
                        }
                    });
                }
            });
        }
        else {
            console.log('Going back to index')
            res.json({
                status_message: obj.status_message
            });
        }
    });
});
/*Create New poll*/
router.post('/api/createPoll', function (req, res, next) {
    var params = req.body;
    var db = req.db;
    var poll_id = Math.floor(Math.random() * 100000 + 1) + 0;
    var options = {
        options_id: (poll_id + poll_id) / 10,
        option_name: 'Testing' + (poll_id + poll_id) / 10,
        options_value: 'Testing' + (poll_id + poll_id) / 10
    }
    var collection = db.get('polls');
    var poll = {
        poll_id: poll_id,
        poll_name: 'Testing Poll' + poll_id,
        poll_options: options,
        poll_winner: {},
        start_date: Date.now(),
        end_date: Date.now() + 30,
        poll_status: 'Active'
    }

    collection.insert(poll);
    collection.find({},function(e,docs){
        res.json(docs);
    })

});

router.get('/api/polls/', function (req, res) {
    var params = req.body;
    var db = req.db;
    console.log('In get polls...' + params.poll_id)
    var collection = db.get('polls');

    var data = [];
    collection.find({}, function (e, docs) {
        if (docs) {
            for (var d in docs) {
                data[d] = {
                    poll_no: parseInt(d) + 1,
                    poll_id: docs[d].poll_id,
                    poll_name: docs[d].poll_name,
                    start_date: docs[d].start_date,
                    end_date: docs[d].end_date,
                    poll_status: docs[d].poll_status
                }
            }
            res.json(data);
        } else
            res.send('No results');

        //collection.findOne({'poll_id': params.poll_id}, function (e, results) {
        //    if (results != null) {
        //        console.log(results);
        //        res.json(results);
        //    }
        //    else {
        //        res.send("Error");
        //    }
    })

});

module.exports = router;